package com.company;

import java.io.Serializable;

/**
 * Creates a Tuple which contains two values
 */
public class Tuple<X, Y> implements Serializable {
    public X x;
    public Y y;

    public Tuple(X x, Y y){
        this.x = x;
        this.y = y;
    }

}
