package com.company;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * ObjectFactory handles all operations connected with creating new figures and drawing them
 */
public class ObjectFactory extends JFrame{
    private int figureX, figureY, figureWidth, figureHeight;
    int windowWidth, windowHeight;
    Color color;
    Shape figure;
    private List<Shape> figures = new ArrayList<>();
    private Map<Shape, Color> figureMapper = new HashMap<>();
    private Map<Shape, Tuple<Integer, Integer>> figuresDim = new HashMap<>();


    /**
     * Creates environment for drawing figures.
     * @throws IOException
     */
    public ObjectFactory() throws IOException { // Inicjalizacja FileManagera
        super();
        System.out.println("Drawing window has been initialized");
        this.setSize(500, 500);
        this.setTitle("Drawing figures");
        this.setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    /**
     *
     * @param g
     */
    public void paint(Graphics g){
        super.paint(g);
        for(Shape figure: this.figures) {
            Graphics2D g2 = (Graphics2D) g.create(); // pracuje na nowym obiekcie, w innym przypadku powoduje to zle skalowanie poprzez wielkorotne monozenie tej samej macierzy przeksztalcen, metoda scale opiera sie na macierzy i jest wredna
            Tuple dim = this.figuresDim.get(figure); // zachowuje wymiary okna, na ktorym zostala stworzona figura
            Color color = figureMapper.get(figure); // Zachowuje color figury
            g2.scale((double)getWidth()/(int)dim.x, (double)getHeight()/(int)dim.y); // Skaluje figure w zaleznosci od wielkosci okna
            g2.setColor(color);
            g2.draw(figure);
            System.out.printf("[D]DRAWING: %s\n", figure.getBounds2D());
        }
    }


    /**
     *Generates different shapes based on random int and generateRandomValues();
     */
    public void generateShape(){
        int shapeSelector = (int)(Math.random() * 3);
        generateRandomValues();

        switch (shapeSelector){
            case 0:
                this.figure = new Ellipse2D.Double(this.figureX, this.figureY, this.figureWidth, this.figureHeight);
            case 1:
                this.figure = new Rectangle2D.Double(this.figureX, this.figureY, this.figureWidth, this.figureHeight);
            case 2:
                this.figure = new Line2D.Double(this.figureX, this.figureY, this.figureWidth, this.figureHeight);
            default:
                this.figure = new Ellipse2D.Double(this.figureX, this.figureY, this.figureWidth, this.figureHeight);

        }

        this.figureMapper.put(this.figure, this.color);
        this.figuresDim.put(this.figure, new Tuple<>(getWidth(), getHeight()));
        this.figures.add(figure);
        System.out.printf("ADDING: %s\n", figure.getBounds2D());
    }

    /**
     *Generates random dimensions and position for new figures
     */
    public void generateRandomValues(){

        this.windowWidth = getWidth();
        this.windowHeight = getHeight();

        this.figureWidth = (int)(Math.random() * windowWidth/2) + 1;
        this.figureHeight = (int)(Math.random() * windowHeight/2) + 1;

        this.figureX = (int)(Math.random() * (windowWidth - 2 * figureWidth)) + figureWidth;
        this.figureY = (int)(Math.random() * (windowHeight - 2 * figureHeight)) + figureHeight;

        this.color = new Color((int)(Math.random() * 0x1000000));

        System.out.println("Figure position and dimension has been generated");
    }
    /**
     *Returns wrapped figures info.
     */
    public FiguresWrapper getFigures(){
        return new FiguresWrapper(List.copyOf(this.figures), Map.copyOf(this.figureMapper), Map.copyOf(this.figuresDim));
    }


}



