package com.company;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.List;
import java.io.IOException;
import java.util.Map;

/**
 * This class handles operations connected with showing figures which are stored in file
 *
 */
public class ObjectImplementation extends JFrame {
    private FiguresWrapper loadedFigures;
    public ObjectImplementation(FiguresWrapper figures) throws IOException {
        System.out.println("Show window has been initialized");
        this.loadedFigures = figures;
        this.setSize(500, 500);
        this.setTitle("Showing figures");
        this.setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    /**
     * Updates information about figures
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void updateFigures() throws IOException, ClassNotFoundException, InterruptedException {
        this.loadedFigures = new FileManager().loadFile();
        System.out.println("Successfully updated figures!");
    }
    /**
     *
     * @param g
     */
    public void paint(Graphics g){
        List<Shape> figures = this.loadedFigures.wrappedFigures;
        Map<Shape, Color> figureMapper = this.loadedFigures.wrappedFigureMapper;
        Map<Shape, Tuple<Integer, Integer>> figuresDim = this.loadedFigures.wrappedFiguresDim;
        super.paint(g);
        for(Shape figure: figures) {
            Graphics2D g2 = (Graphics2D) g.create(); // pracuje na nowym obiekcie, w innym przypadku powoduje to zle skalowanie poprzez wielkorotne monozenie tej samej macierzy przeksztalcen
            Tuple dim = figuresDim.get(figure);
            Color color = figureMapper.get(figure);
            g2.scale((double)getWidth()/(int)dim.x, (double)getHeight()/(int)dim.y);
            g2.setColor(color);
            g2.draw(figure);
            System.out.printf("[S]DRAWING: %s\n", figure.getBounds2D());

        }
    }
}
