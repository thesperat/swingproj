package com.company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class Main {
    /**
     *Muszę zrobić dwa wątki dające możłiwość dostępu jednoczesnego do jegnego pliku i na tej postawie jego rysowania.
     */
    public static void main(String[] args) {
        try {
            ObjectFactory frame = new ObjectFactory();
            FileManager manager = new FileManager();

            Timer t1 = new Timer(1000, (ActionEvent e ) -> {
                frame.generateShape();
                try {
                    manager.writeToFile(frame.getFigures());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                frame.repaint();
            } );
            t1.start();

            File f = new File(manager.getPath());
            while(f.length() == 0){
                Thread.sleep(500); // wait until file has been fed with data
            }

            ObjectImplementation frame2 = new ObjectImplementation(manager.loadFile());

            Timer t2 = new Timer(2000, (ActionEvent e ) -> {
                try {
                    frame2.updateFigures();
                } catch (IOException | ClassNotFoundException | InterruptedException e1) {
                    e1.printStackTrace();
                }
                frame2.repaint();

            } );

            t2.start();

        } catch (IOException | ClassNotFoundException | InterruptedException e) {
            e.printStackTrace();
        }


    }
}



