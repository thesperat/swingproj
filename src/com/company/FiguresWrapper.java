package com.company;

import java.awt.*;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Wrapps all information about figures allowing to save those information to file
 */
public class FiguresWrapper implements Serializable {
    private static final long serialVersionUID = 1L;
    public List<Shape> wrappedFigures;
    public Map<Shape, Color> wrappedFigureMapper;
    public Map<Shape, Tuple<Integer, Integer>> wrappedFiguresDim;

    public FiguresWrapper(List<Shape> figures, Map<Shape, Color> figureMapper, Map<Shape, Tuple<Integer, Integer>> figuresDim){
        this.wrappedFigures = figures;
        this.wrappedFigureMapper = figureMapper;
        this.wrappedFiguresDim = figuresDim;
    }
}
