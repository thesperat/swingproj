package com.company;
import java.util.List;
import java.awt.*;
import java.io.*;
import java.util.Map;

public class FileManager{
    private String path = "\\Users\\TheSperat\\Documents\\gui1";

    /**
     * writeToFile handles all actions connected with saving figures information as class object
     * directly to the file. That object has three different attributes which describes figures
     * attributes.
     *
     * @param
     * @throws IOException
     */

    public void writeToFile(FiguresWrapper fw) throws IOException{
        FileOutputStream fileOut = new FileOutputStream(this.path);
        ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
        objectOut.writeObject(fw);
        objectOut.close();
        System.out.println("Wrapped object was successfully written to a file");
    }

    /**
     * loadFile obtains FiguresWrapper object from file and returns it allowing to access data
     * for other methods.
     *
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public FiguresWrapper loadFile() throws IOException, ClassNotFoundException {

        FileInputStream fi = new FileInputStream(this.path);
        ObjectInputStream oi = new ObjectInputStream(fi);
        System.out.println("Wrapped object was successfully obtained from a file");
        return (FiguresWrapper)oi.readObject();
    }
    public String getPath(){
        return this.path;
    }
}


